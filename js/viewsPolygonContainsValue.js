/**
 * @file
 * Used by "Polygon Contains Points" contextual filter.
 * Geojoin geojoin_views_handler_argument_polygoncontains implementation.
 *
 * On views form widget for "Polygon Contain Points" switched out
 * the form fields depending on source selection e.g.
 * (Entity from URL | Mysql GIS, Entity from URL | PostGIS, etc..)
 */
(function ($) {
  Drupal.behaviors.viewsPolygonContainsValue = {
    attach: function (context, settings) {
      if (!$('body').hasClass('page-admin-structure-views-nojs')) {
        $('#edit-options-source-change').hide();
      }
      $('#edit-options-source').change(function() {
        $('#edit-options-source-change').mousedown();
        $('#edit-options-source-change').submit();
      });
    }
  };
})(jQuery);