<?php

/**
 * @file
 * Geojoin views_handler_relationship_geojoin implementation.
 */

class views_handler_relationship_geojoin extends views_handler_relationship  {
  var $dbtype;
  var $geomtype = 'wkt';

  function option_definition() {
    $options = parent::option_definition();
    $options['relation_operator'] = array('default' => !empty($this->definition['relation_operator']) ? $this->definition['relation_operator'] : 'overlaps');
    $options['exclude_self'] = array('default' => !empty($this->definition['exclude_self']) ? $this->definition['exclude_self'] : 1);
   return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // this is draft, un-used code. to try to figure out multi-entity crosses
    $bundle = $this->definition['base'];
    $field_options = array();
    $entities = entity_get_info('dh_feature');
    $fields = $entities['bundles'];
    foreach ($fields as $bundle => $info) {
      $field_options[$bundle] = $info['label'];
    }
    // end draft, un-used code.
    
    $functions = array(
      'overlaps' => 'A Overlaps B',
      'intersects' => 'A Intersects B',
      'contains' => 'A Contains B',
      'within' => 'A Within B',
      'contains_centroid' => 'A Contains Centroid of B',
      'centroid_within' => 'Centroid of A Within B',
      'touches' => 'A Touches B',
    );
    $form['relation_operator'] = array(
      '#type' => 'select',
      '#title' => t('Spatial Relationship'),
      '#options' => $functions,
      '#default_value' => $this->options['relation_operator'],
    );
    $exclude_self = array(
      1 => 'TRUE',
      0 => 'FALSE',
    );
    $form['exclude_self'] = array(
      '#type' => 'select',
      '#title' => t('Exclude Self?'),
      '#options' => $exclude_self,
      '#default_value' => $this->options['exclude_self'],
    );
    // @todo: add switch to enable including feature A in returned results (would add OR to join conditions to include feature A)
    // @todo: add proximity to join, with ability to use arguments as distance parameter
    // Remove the 'require':
    unset($form['required']);
  }

  function query() {
    $this->ensure_my_table();
    $def = $this->definition;
    //$def += array('exclude_self' => TRUE); 
    //drupal_set_message("Relationship DEF = <pre>" . print_r($def,1) . "</pre>");
      // 1. Verify that dh_feature class is present
    // 2. Create First link to dh_geofield based on entity_id
    // 3. Add geospatial join to other dh_geofield entries (e1.entity_id <> e2.entity_id)
    // 4. join 2nd dh_geofield table to it's related dh_feature content
    // 5. Set this handler alias to the final destination feature table
    $dbtype = Database::getConnection()->databaseType();
    switch ($dbtype) {
      case 'pgsql':
        $this->dbtype = 'pgsql';
        $value = db_query('SELECT substring(PostGIS_Version() from 1 for 3)')->fetchField();
        if (empty($value)) {
          $error = 'Could not detect postGIS version';
          drupal_set_message($error);
          return FALSE;
        }
        $this->dbtype = 'postgis';
        $this->spatial_version = floatval($value);
      break;
      case 'mysql':
        $value = db_query("SELECT asText(geomfromtext('POINT(1 1)'))")->fetchField();
        if (empty($value)) {
          $error = 'Could not detect OpenGIS Functions';
          drupal_set_message($error);
          return FALSE;
        }
        $version = db_query("SELECT version()")->fetchField();
        if (empty($version)) {
          $this->spatial_version = 5.0;
        } else {
          $this->spatial_version = floatval($version);
        }
        $this->dbtype = 'mysql';
        $this->geomtype = 'wkb';
      break;
    }
    // get base entity spatial join
    $join = new views_join;
    $baseid = $def['field'];
    $featuretab = $def['table'];
    $geomid = 'entity_id';
    $geomtable = 'field_data_' . $def['geofield'];
    $gt_prefix = $def['geofield'];
    $info = field_info_field($gt_prefix);
    $this->srid = $info['settings']['srid'];
    //dpm($info,"Geo Field Info");
    if ($info['columns']['geom']['type'] == 'blob') {
       $this->geomtype = 'wkb';
    }
    // START borrowed from GeoField PostGIS
    // I share the WTF?: @@TODO: is there a better way to find this info?
    $db_info = $info['storage']['details']['sql']['FIELD_LOAD_CURRENT'];
    // end borrowed from GeoField PostGIS
    $table = current(array_keys($db_info));
    switch ($this->geomtype) {
      case 'wkt':
      $gckey = 'wkt';
      break;
      case 'wkb':
      $gckey = 'geom';
      break;
      case 'wkt':
      $gckey = 'wkt';
      break;
    }
    $geocolumn = $db_info[$table][$gckey];
    $gt_srid = $gt_prefix . '_srid';
    $reltype = $this->options['relation_operator'];
    $join->construct($geomtable, $featuretab, $baseid, $geomid);
    $res = $this->query->add_relationship($geomtable, $join, $geomtable, $featuretab);

    // geospatial join to other entities
    $res2 = $geomtable . "_2";
    $join = $this->geojoin_relate($reltype, $res, $res2, $geocolumn, $geocolumn, $gt_srid, $gt_srid);
    $res2 = $this->query->add_relationship($res2, $join, $geomtable);

    // Get the joined features base
    $join = new views_join;
    $join->construct($featuretab, $res2, $geomid, $baseid);
    // compatible with 60 char aliases
    $f2 = $featuretab;
    // commented to add cpompatibility with 60 char aliases
    //$f2 = $featuretab . "_2";
    // THIS IS CRITICAL -- Adding a relationship is necessary because it tells the
    // Drupal query plumbing that you don't want to re-use existing join paths
    $join->adjusted = TRUE;
    $res3 = $this->query->add_relationship($f2, $join, $featuretab, $this->relationship);
    // Set this handler alias to the final destination feature table
    // compatible with 60 char aliases
    $this->alias = $this->query->add_table($res3, $res2, $join, $res3);
    // commented to add cpompatibility with 60 char aliases
    //$this->alias = $this->query->add_table($f2, $res2, $join, $res3);

    // 6. Set the where clause to screen on selected bundles
      if (isset($this->options['left_feature'])) {
         $this->query->add_where_expression(0, $featuretab . ".bundle = '" . $this->options['left_feature'] . "'");
    }
      if (isset($this->options['right_feature'])) {
         $this->query->add_where_expression(0, $f2 . ".bundle = '" . $this->options['right_feature'] . "'");
    }
    $def = $this->definition;
  }
  
  function geojoin_relate($reltype, $geobase, $target, $base_field, $target_field, $base_srid, $target_srid) {
    // assess db specific function prefixes
    switch ($this->dbtype) {
      case 'postgis':
      if ($this->spatial_version > 2.0) {
        $fn_prefix = "st_";
      } else {
        $fn_prefix = "";
      }
      break;
      case 'mysql':
      if ($this->spatial_version >= 5.6) {
        $fn_prefix = "st_";
      } else {
        $fn_prefix = "";
      }
      break;

      case 'pgsql':
      $fn_prefix = "";
      break;

      default:
      $fn_prefix = "";
      break;
    }

    // handle db specific preparation of column for use in spatial functions
    switch ($this->geomtype) {
      case 'wkt':
      $base_geom = "GeomFromText($geobase." . "$base_field)";
      $targ_geom = "GeomFromText($target." . "$target_field)";
      break;
      case 'wkb':
      switch ($this->dbtype) {
        case 'mysql':
          $base_geom = "geomFromWKB($geobase." . "$base_field)";
          $targ_geom = "geomFromWKB($target." . "$target_field)";
        break;
        case 'postgis':
          if ($this->srid) {
            $base_geom = $fn_prefix . "setsrid($geobase." . "$base_field, $this->srid)";
            $targ_geom = $fn_prefix . "setsrid($target." . "$target_field, $this->srid)";
          }
        break;
        default:
          $base_geom = "$geobase." . "$base_field";
          $targ_geom = "$target." . "$target_field";
        break;
      }
      break;
    }

    // assess db specific join pre-clause, useful for mandatin indicies in postgis
    switch ($this->dbtype) {
      case 'postgis':
      $join_prefix = " ( $base_geom && $targ_geom ) ";
      break;
      case 'mysql':
      $join_prefix = " ( 1 = 1) ";
      break;
      case 'pgsql':
      $join_prefix = " ( 1 = 1) ";
      break;
      default:
      $join_prefix = " ( 1 = 1) ";
      break;
    }
    switch ($this->options['exclude_self']) {
      case 0:
      case FALSE:
      $identity_filter = "(1 = 1)";
      break;
      
      default:
      $identity_filter = "$geobase.entity_id <> $target.entity_id";
      break;
    }
    switch ($reltype) {
      case 'overlaps':
        $geojoin = " ( $join_prefix AND $identity_filter and ";
        switch ($this->dbtype) {
          case 'postgis':
          $geojoin .= " $join_prefix ";
          break;

          default:
          $fn = $fn_prefix . 'overlaps';
          $geojoin .= "$fn($base_geom, $targ_geom ) ";
          break;
        }
        $geojoin .= ")";
      break;

      case 'intersects':
        $fn = $fn_prefix . 'intersects';
        $geojoin = " ( $join_prefix AND $identity_filter and $fn($base_geom, $targ_geom) )";
      break;

      case 'contains':
        $fn = $fn_prefix . 'contains';
        $geojoin = " ( $join_prefix AND $identity_filter and $fn($base_geom, $targ_geom) )";
      break;

      case 'contains_centroid':
        $fn = $fn_prefix . 'contains';
        $geojoin = " ( $join_prefix AND $identity_filter and $fn($base_geom, st_centroid($targ_geom)) )";
      break;

      case 'within':
        $fn = $fn_prefix . 'within';
        $geojoin = " ( $join_prefix AND $identity_filter and $fn($base_geom, $targ_geom) )";
      break;

      case 'centroid_within':
        $fn = $fn_prefix . 'within';
        $geojoin = " ( $join_prefix AND $identity_filter and $fn(st_centroid($base_geom), $targ_geom) )";
      break;

      case 'touches':
        $fn = $fn_prefix . 'touches';
        $geojoin = " ( $join_prefix AND $identity_filter and $fn($base_geom, $targ_geom) )";
      break;

      default:
        $geojoin = " ( $join_prefix AND $identity_filter and $base_geom && $targ_geom )";
      break;
    }
    $join = new views_join();
    //$join->construct($geobase, $target, $base_srid, $target_srid, $geojoin);
    $join->construct($geobase, $target, 'deleted', 'deleted', $geojoin);
    return $join;
  }
}

/**
 * Builds a inner join without the default ON clause. Instead, we replace it
 * with a user-defined ON equality.
 */
/**
 * Builds a inner join without the default ON clause. Instead, we replace it
 * with a user-defined ON equality.
 */
class geojoin_join extends views_join {

  /**
   * Build the SQL for the join this object represents.
   *
   * When possible, try to use table alias instead of table names.
   *
   * @param $select_query
   *   An implementation of SelectQueryInterface.
   * @param $table
   *   The base table to join.
   * @param $view_query
   *   The source query, implementation of views_plugin_query.
   */
  // RIGHT NOW THIS IS A CLONE OF views_join EXCEPT IT USES (1 = 1) AS INITIAL CONDITION
  // NOT THE BEST APPROACH - NEED TO FIX RWB
  function build_join($select_query, $table, $view_query) {
    if (empty($this->definition['table formula'])) {
      $right_table = $this->table;
    }
    else {
      $right_table = $this->definition['table formula'];
    }

    if ($this->left_table) {
      $left = $view_query->get_table_info($this->left_table);
      $left_field = "$left[alias].$this->left_field";
    }
    else {
      // This can be used if left_field is a formula or something. It should be used only *very* rarely.
      $left_field = $this->left_field;
    }

    $condition = " 1 = 1 ";
    $arguments = array();

    // Tack on the extra.
    if (isset($this->extra)) {
      if (is_array($this->extra)) {
        $extras = array();
        foreach ($this->extra as $info) {
          $extra = '';
          // Figure out the table name. Remember, only use aliases provided
          // if at all possible.
          $join_table = '';
          if (!array_key_exists('table', $info)) {
            $join_table = $table['alias'] . '.';
          }
          elseif (isset($info['table'])) {
            // If we're aware of a table alias for this table, use the table
            // alias instead of the table name.
            if (isset($left) && $left['table'] == $info['table']) {
              $join_table = $left['alias'] . '.';
            }
            else {
              $join_table = $info['table'] . '.';
            }
          }

          // Convert a single-valued array of values to the single-value case,
          // and transform from IN() notation to = notation
          if (is_array($info['value']) && count($info['value']) == 1) {
            if (empty($info['operator'])) {
              $operator = '=';
            }
            else {
              $operator = $info['operator'] == 'NOT IN' ? '!=' : '=';
            }
            $info['value'] = array_shift($info['value']);
          }

          if (is_array($info['value'])) {
            // With an array of values, we need multiple placeholders and the
            // 'IN' operator is implicit.
            foreach ($info['value'] as $value) {
              $placeholder_i = ':views_join_condition_' . $select_query->nextPlaceholder();
              $arguments[$placeholder_i] = $value;
            }

            $operator = !empty($info['operator']) ? $info['operator'] : 'IN';
            $placeholder = '( ' . implode(', ', array_keys($arguments)) . ' )';
          }
          else {
            // With a single value, the '=' operator is implicit.
            $operator = !empty($info['operator']) ? $info['operator'] : '=';
            $placeholder = ':views_join_condition_' . $select_query->nextPlaceholder();
            $arguments[$placeholder] = $info['value'];
          }

          $extras[] = "$join_table$info[field] $operator $placeholder";
        }

        if ($extras) {
          if (count($extras) == 1) {
            $condition .= ' AND ' . array_shift($extras);
          }
          else {
            $condition .= ' AND (' . implode(' ' . $this->extra_type . ' ', $extras) . ')';
          }
        }
      }
      elseif ($this->extra && is_string($this->extra)) {
        $condition .= " AND ($this->extra)";
      }
    }

    $select_query->addJoin($this->type, $right_table, $table['alias'], $condition, $arguments);
  }

}
