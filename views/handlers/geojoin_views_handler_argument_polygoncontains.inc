<?php

/**
 * @file
 * Geojoin contextual filter argument handler for Views.
 * Geojoin geojoin_views_handler_argument_polygoncontains implementation.
 */

/**
 * @todo Add Description
 */
class geojoin_views_handler_argument_polygoncontains extends views_handler_argument {

  function option_definition() {
    $options = parent::option_definition();
    $options['source'] = array('default' => '');
    $polygonContainsHandlers = module_invoke_all('polygon_contains_views_handlers');
    foreach ($polygonContainsHandlers as $key => $handler) {
      if (class_exists($handler['class'])) {
        $polygonContains = new $handler['class']();
        $polygonContains->option_definition($options, $this);
      }
    }
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['source'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Source of Polygon Shape Data'),
      '#description' => t('How do you want to enter your polygon shape data?'),
      '#options' => array('' => ''),
      '#default_value' => $this->options['source'],
      '#attached' => array(
        'js' => array(
          drupal_get_path('module', 'geojoin') . '/js/viewsPolygonContainsValue.js',
        ),
      ),
    );
    $polygonContainsHandlers = module_invoke_all('polygon_contains_views_handlers');
    //dpm($polygonContainsHandlers, "polygonContainsHandlers");
    foreach ($polygonContainsHandlers as $key => $handler) {
      $form['source']['#options'][$key] = $handler['name'];
      if (class_exists($handler['class'])) {
        $polygonContainsPlugin = new $handler['class']();
        $polygonContainsPlugin->options_form($form, $form_state, $this);
      }
    }
  }

  /**
   * Set up the where clause for the contextual filter argument.
   */
  function query($group_by = FALSE) {
    if (empty($this->options['source'])) {
      return;
    }
    $selected_source = $this->options['source'];
    $polygonContainsHandlers = module_invoke_all('polygon_contains_views_handlers');
    if (empty($polygonContainsHandlers[$selected_source])) {
      return;
    }
    if (!class_exists($polygonContainsHandlers[$selected_source]['class'])) {
      return;
    }
    if (empty($this->argument)) {
      return;
    }
    // get chosen db backend type chosen by user.
    $polygonContainsPlugin = new $polygonContainsHandlers[$selected_source]['class']();
    $polygonContainsPlugin->query($group_by, $this);
    // $group = $this->query->set_where_group('AND');
    // $this->query->add_where($group, 'node.sticky', 1, '<=');
  }

}
