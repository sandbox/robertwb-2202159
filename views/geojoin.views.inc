<?php

/**
 * @file
 * Hooks for Views integration.
 *
 * Based on geofield implementation..
 * In addition to the usual views hooks, Geojoin defines a set of classes that allows
 * us to break out the code needed to handle the wide variety of proximity searches
 * the module supports. We define api functions here to allow for very basic autoloading.
 *
 * Our mini-plugin system we're using here is not designed for external (non-geojoin) use,
 * but as a programming convenience for the maintainers.
 *
 */

/**
 * Implements hook_view_data_alter().
 * @category views_handler_relationship_geojoin
 */
function geojoin_views_data_alter(&$data) {
  $data['geojoin']['table']['group'] = t('GeoJoin: GeoSpatial Joins');
  $data['geojoin']['table']['join'] = array(
    '#global' => array(),
  );
  $fields = field_info_field_map();
  error_log("Searching for geofield instances ");
  foreach ($fields as $name => $props) {
    if ($props['type'] == 'geofield') {
      error_log("Found geofield as field $name " . print_r($props, 1));
      foreach ($props['bundles'] as $key => $bundle) {
        if (is_array($bundle)) {
          $bundle = $key;
        }
        error_log("Found geofield as bundle $bundle ");
        $einfo = entity_get_info($bundle);
        $table = $einfo['base table'];
        if (isset($einfo['entity keys']['id'])) {
          error_log("Found 'entity keys' -> 'id' on bundle $bundle ");
          $data['geojoin'][$bundle . "_$name"]['title'] = t("GeoJoin: $bundle Spatial Relationship for Entities with Field $name");
          $data['geojoin'][$bundle . "_$name"]['help'] = t('Do two shapes relate spatially?');
          $data['geojoin'][$bundle . "_$name"]['relationship'] = array(
            'handler' => 'views_handler_relationship_geojoin',
            'base' => $bundle,
            'table' => $table,
            'field' => $einfo['entity keys']['id'],
            'geofield' => $name,
            // think this "skip base" is necessary to permit self-join?
            'skip base' => array($bundle),
            'label' => t('@table Elements', array('@table' => "GeoJoin: $bundle Spatial Relationship for Entities with Field $name"))
          );
          // Now add one to the table itself instead of this psuedo geojoin table.
          // this should allow the "relationship" field?
          // ultimately should we delete the above version if this works?
          // how to update all views?
          $data[$table][$bundle . "_$name"]['title'] = t("GeoJoin: $bundle Spatial Relationship for Entities with Field $name");
          $data[$table][$bundle . "_$name"]['help'] = t('Do two shapes relate spatially?');
          $data[$table][$bundle . "_$name"]['relationship'] = array(
            'handler' => 'views_handler_relationship_geojoin',
            'base' => $bundle,
            'table' => $table,
            'field' => $einfo['entity keys']['id'],
            'geofield' => $name,
            // think this "skip base" is necessary to permit self-join?
            //'skip base' => array($table),
            'label' => t('@table Elements', array('@table' => "(new) GeoJoin: $bundle Spatial Relationship for Entities with Field $name"))
          );
        }
      }
    }
  }
}

/**
 * Implements hook_views_handlers().
 */
function geojoin_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'geojoin') . '/views',
    ),
    'handlers' => array(
      'geojoin_views_handler_argument_polygoncontains' => array(
        'parent' => 'views_handler_argument',
      )
    ),
  );
}

/**
 * Implements hook_views_data().
 *
 * @category geojoin_views_handler_argument_polygoncontains
 *   Adds "Polygon Contains Points" contextual filter.
 */
function geojoin_views_data() {
  $data = array();
  $data['views']['polygon_contains'] = array(
    'group' => t('Mapping'),
    'real field'  => 'polygon_contains_argument',
    'title' => t('Polygon Contains Points'),
    'help' => t('Filter locations/points within a polygon.'),
    'argument' => array(
      'handler' => 'geojoin_views_handler_argument_polygoncontains'
    ),
  );
  return $data;
}


/**
 * Implements hook polygon_contains_views_handlers().
 *
 * Returns metainfo about each of the geojoin polygon contains views classes.
 *
 * @internal mysql-note: at the time of this build , found out that even if
 * the site install has mysql 5.6 and above, geofield might still
 * not use spatial mysql columns but instead relying on longblob and geophp
 *
 * as a contigency for sites stuck with table and columns without using
 * mysql spatial even if the actual db is spatial capable; users
 * are given the option to make use of mysql-geophp
 *
 * @internal geophp-note: geoPHP advanced methods , specifically "contains" needs GEOS
 *
 * @category geojoin_views_handler_argument_polygoncontains
 *   Adds "Polygon Contains Points" contextual filter.
 */
function geojoin_polygon_contains_views_handlers() {
  $handlers = array();
  $dbtype = '';
  $dbversion = 0;
  $dbtype = Database::getConnection()->databaseType();
  if ($dbtype == 'mysql') {
    $dbversion = db_query('SELECT version()')->fetchField();
    $dbversion = floatval($dbversion);
  }
  if (($dbtype == 'mysql') && ($dbversion >= 5.6)) {
    $handlers['mysqlgis_entity_from_url'] = array(
      'name' => t('Entity From URL | MYSQL GIS Support'),
      'class' => 'geojoinMysqlGisPolygonContainsEntityURL',
      'module' => 'geojoin',
      'path' => 'views/polygon_contains_plugins',
    );
  }
  if (($dbtype == 'pgsql')) {
    $value = db_query('SELECT substring(PostGIS_Version() from 1 for 3)')->fetchField();
    $geotype = '';
    if (!empty($value)) {
      $geotype = 'postgis';
    }
    
    $handlers['postgis_entity_from_url'] = array(
      'name' => t('Entity From URL | PostGIS Support'),
      'class' => 'geojoinPostGisPolygonContainsEntityURL',
      'module' => 'geojoin',
      'path' => 'views/polygon_contains_plugins',
    );
  }
  $geophp = geophp_load();
  // geoPHP advanced methods , specifically "contains" needs GEOS
  if (geoPHP::geosInstalled()) {
    $handlers['mysqlgeophp_entity_from_url'] = array(
      'name' => t('Entity From URL | MYSQL with geoPHP'),
      'class' => 'geojoinMysqlGeophpPolygonContainsEntityURL',
      'module' => 'geojoin',
      'path' => 'views/polygon_contains_plugins',
    );
  }
  return $handlers;
}

/**
 * Loads an individual geojoin polygon contains views class.
 *
 * @return
 *   An instance of a class defined by $plugin (see keys for geojoin_polygon_contains_views_handlers), or FALSE
 *   if no such class exists.
 * @category geojoin_views_handler_argument_polygoncontains
 *   Adds "Polygon Contains Points" contextual filter.
 */

function geojoin_proximity_load_plugin($plugin) {
  $handlers = module_invoke_all('polygon_contains_views_handlers');
  //module_load_include('inc', 'geofield', 'views/proximity_plugins/geofieldProximityManual');
  drupal_set_message("Trying to load plugin: $plugin");
  module_load_include('inc', $handlers[$plugin]['module'], $handlers[$plugin]['path'] . '/' . $handlers[$plugin]['class']);
  if (class_exists($handlers[$plugin]['class'])) {
    return new $handlers[$plugin]['class'];
  }
  else {
    return FALSE;
  }
}
