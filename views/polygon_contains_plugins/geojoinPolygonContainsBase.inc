<?php

/**
 * @file
 *   Contains geojoinPolygonContainsPluginInterface and geojoinPolygonContainsBase.
 * @category geojoin_views_handler_argument_polygoncontains
 *   Adds "Polygon Contains Points" contextual filter.
 */

interface geojoinPolygonContainsPluginInterface {
  /**
   * All methods in geojoinPolygonContainsPluginInterface maps directly to a
   * method in a views_handler class,
   *
   * @todo check if comment section needed.
   * expect for 'getSourceValue,' which
   * is primarily called in the 'query' method, but also in other instances.
   */
  public function option_definition(&$options, $views_plugin);
  public function options_form(&$form, &$form_state, $views_plugin);
  public function options_validate(&$form, &$form_state, $views_plugin);
  public function value_form(&$form, &$form_state, $views_plugin);
  public function value_validate(&$form, &$form_state, $views_plugin);
  public function query($group, &$views_plugin);
}

class geojoinPolygonContainsBase implements geojoinPolygonContainsPluginInterface {
  public function option_definition(&$options, $views_plugin) {

  }

  public function options_form(&$form, &$form_state, $views_plugin) {

  }

  public function options_validate(&$form, &$form_state, $views_plugin) {

  }

  public function value_form(&$form, &$form_state, $views_plugin) {
  }

  public function value_validate(&$form, &$form_state, $views_plugin) {

  }

  public function query($group, &$views_plugin) {

  }
}
