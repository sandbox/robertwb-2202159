<?php

/**
 * @file
 * Contains geojoinPolygonContainsEntityURL.
 * @category geojoin_views_handler_argument_polygoncontains
 *   Adds "Polygon Contains Points" contextual filter.
 */

class geojoinMysqlGeophpPolygonContainsEntityURL extends geojoinPolygonContainsBase implements geojoinPolygonContainsPluginInterface {
  public function option_definition(&$options, $views_plugin) {
    $options['mysqlgeophp_entity_from_url_entity_type'] = array(
      'default' => 'node',
    );
    $options['mysqlgeophp_entity_from_url_field'] = array(
      'default' => '',
    );
    $options['mysqlgeophp_entity_from_url_entity_targetbundle'] = array(
      'default' => '',
    );
    $options['mysqlgeophp_entity_from_url_targetfield'] = array(
      'default' => '',
    );
  }

  public function options_form(&$form, &$form_state, $views_plugin) {
    $entities = entity_get_info();
    $entity_options = array();
    foreach ($entities as $key => $entity) {
      $entity_options[$key] = $entity['label'];
    }

    $geofields = _geofield_get_geofield_fields();
    $field_options = array();
    foreach ($geofields as $key => $field) {
      $field_options[$key] = $key;
    }

    $form['mysqlgeophp_entity_from_url_entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity Type, Polygon'),
      '#required' => TRUE,
      '#description' => 'Polygon will be derived from the Entity from url',
      '#default_value' => $views_plugin->options['mysqlgeophp_entity_from_url_entity_type'],
      '#options' => $entity_options,
      '#dependency' => array(
        'edit-options-source' => array('mysqlgeophp_entity_from_url'),
      ),
    );

    $form['mysqlgeophp_entity_from_url_field'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Source Field, Polygon'),
      '#default_value' => $views_plugin->options['mysqlgeophp_entity_from_url_field'],
      '#options' => $field_options,
      '#dependency' => array(
        'edit-options-source' => array('mysqlgeophp_entity_from_url'),
      ),
    );

    $base_table = $views_plugin->view->base_table;
    $target_bundle_entity_options = array();
    // one a view that is based on node, the points should be based only on node.
    foreach ($entities[$base_table]['bundles'] as $bundle_name => $bundle) {
      $target_bundle_entity_options[$bundle_name] = $bundle['label'];
    }

    $form['mysqlgeophp_entity_from_url_entity_targetbundle'] = array(
      '#type' => 'select',
      '#title' => t('Bundle, Points'),
      '#description' => t('The view is based on ' . $base_table . ' so are the Points Entity Type'),
      '#required' => TRUE,
      '#default_value' => $views_plugin->options['mysqlgeophp_entity_from_url_entity_targetbundle'],
      '#options' => $target_bundle_entity_options,
      '#dependency' => array(
        'edit-options-source' => array('mysqlgeophp_entity_from_url'),
      ),
    );

    $form['mysqlgeophp_entity_from_url_targetfield'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Source Field, Points'),
      '#default_value' => $views_plugin->options['mysqlgeophp_entity_from_url_targetfield'],
      '#options' => $field_options,
      '#dependency' => array(
        'edit-options-source' => array('mysqlgeophp_entity_from_url'),
      ),
    );
  }

  /**
   * Set up the where clause for the contextual filter argument.
   */
  public function query($group_by = FALSE, &$views_plugin) {
    $polygon_entity_type =  $polygon_entity_id = $polygon_field_name = '';
    $points_entity_type = $points_entity_bundle = $points_field_name = '';
    $points_entity_keys = $fields = array();
    if (empty($views_plugin->options['mysqlgeophp_entity_from_url_entity_type'])) {
      return;
    }
    $polygon_entity_type = $views_plugin->options['mysqlgeophp_entity_from_url_entity_type'];
    if (empty($views_plugin->options['mysqlgeophp_entity_from_url_field'])) {
      return;
    }
    $polygon_field_name = $views_plugin->options['mysqlgeophp_entity_from_url_field'];
    if (empty($views_plugin->view->base_table)) {
      return;
    }
    $points_entity_type = $views_plugin->view->base_table;
    if (empty($views_plugin->options['mysqlgeophp_entity_from_url_entity_targetbundle'])) {
      return;
    }
    $points_entity_bundle = $views_plugin->options['mysqlgeophp_entity_from_url_entity_targetbundle'];
    if (empty($views_plugin->options['mysqlgeophp_entity_from_url_targetfield'])) {
      return;
    }
    $points_field_name = $views_plugin->options['mysqlgeophp_entity_from_url_targetfield'];
    if (empty($views_plugin->argument)) {
      return;
    }
    $points_entity_type = check_plain($points_entity_type);
    $points_entity_bundle = check_plain($points_entity_bundle);
    $points_field_name = check_plain($points_field_name);
    $polygon_entity_id = intval($views_plugin->argument);
    $polygon_entities = entity_load($polygon_entity_type, array($polygon_entity_id));
    if (empty($polygon_entities)) {
      return;
    }
    $polygon_entity = reset($polygon_entities);
    $points_entity_info = entity_get_info($points_entity_type);
    if (!empty($points_entity_info['entity keys'])) {
      $points_entity_keys = $points_entity_info['entity keys'];
    }
    $fields = field_info_instances($points_entity_type, $points_entity_bundle);
    $display_fields = array(
      $points_field_name,
    );
    // entity_extract_ids through field_attach_load requires "type" property
    // field_revision tables have type column as entity_type
    $points_entities = db_select('field_revision_'.$points_field_name, 'f')
      ->fields('f', array('entity_id', 'entity_type', 'bundle', 'revision_id'))
      ->condition('entity_type', $points_entity_type)
      ->condition('bundle', $points_entity_bundle)
      ->condition($points_field_name.'_geo_type', 'point')
      ->condition('deleted', 0)
      ->execute()
      ->fetchAll();
    if (empty($points_entity_keys) || empty($fields)) {
      return;
    }
    // mold points entities to field_attach_load expected strucuture.
    // for example on node objects entity_id becomes nid, bundle becomes type, etc..
    $parsed_points_entities = array();
    foreach ($points_entities as $sequence => $points_entity) {
      $parsed_points_entities[$points_entity->entity_id] = new stdClass();
      $parsed_points_entities[$points_entity->entity_id]->{$points_entity_keys['id']} = $points_entity->entity_id;
      $parsed_points_entities[$points_entity->entity_id]->{$points_entity_keys['bundle']} = $points_entity->bundle;
      $parsed_points_entities[$points_entity->entity_id]->{$points_entity_keys['revision']} = $points_entity->revision_id;
    }
    // better than node_load_multiple because of less hook_node_load calls.
    // but this makes sure we have all fields data as specified in display fields.
    $count_fields_attached = 0;
    foreach ($display_fields as $display_field) {
      if (isset($fields[$display_field]['field_id'])) {
        field_attach_load($points_entity_type,
          $parsed_points_entities,
          FIELD_LOAD_CURRENT,
          array('field_id' => $fields[$display_field]['field_id'])
        );
        $count_fields_attached++;
      }
    }
    if (empty($parsed_points_entities) || $count_fields_attached <= 0) {
      return;
    }
    geophp_load();
    $item = $polygon_entity->{$polygon_field_name}[LANGUAGE_NONE][0];
    $polygon = geoPHP::load($item['geom']);
    if ($polygon->isEmpty()) {
      return;
    }
    $ids_inside_polygon = array();
    foreach ($parsed_points_entities as $entity_id => $parsed_points_entity) {
      $item = array();
      $item = $parsed_points_entity->{$points_field_name}[LANGUAGE_NONE][0];
      if (empty($item)) {
        continue;
      }
      $point = geoPHP::load($item['geom']);
      if ($point->isEmpty()) {
        continue;
      }
      if ($polygon->contains($point)) {
        $ids_inside_polygon[] = $entity_id;
      }
    }
    if (empty($ids_inside_polygon)) {
      return;
    }
    $group = $views_plugin->query->set_where_group('AND');
    $views_plugin->query->add_where(
      $group,
      $points_entity_type . '.' . $points_entity_keys['id'],
      $ids_inside_polygon,
      'IN'
    );
  }
}
