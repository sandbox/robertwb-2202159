<?php

/**
 * @file
 * Contains geojoinPolygonContainsEntityURL.
 * @category geojoin_views_handler_argument_polygoncontains
 *   Adds "Polygon Contains Points" contextual filter.
 */

class geojoinMysqlGisPolygonContainsEntityURL extends geojoinPolygonContainsBase implements geojoinPolygonContainsPluginInterface {
  public function option_definition(&$options, $views_plugin) {
    $options['mysqlgis_entity_from_url_entity_type'] = array(
      'default' => 'node',
    );
    $options['mysqlgis_entity_from_url_field'] = array(
      'default' => '',
    );
  }

  public function options_form(&$form, &$form_state, $views_plugin) {
    $entities = entity_get_info();
    $entity_options = array();
    foreach ($entities as $key => $entity) {
      $entity_options[$key] = $entity['label'];
    }

    $form['mysqlgis_entity_from_url_entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity Type'),
      '#default_value' => $views_plugin->options['mysqlgis_entity_from_url_entity_type'],
      '#options' => $entity_options,
      '#dependency' => array(
        'edit-options-source' => array('mysqlgis_entity_from_url'),
      ),
    );
drupal_set_message("Calling _geofield_get_geofield_fields() ");
    $geofields = _geofield_get_geofield_fields();
    $field_options = array();
    foreach ($geofields as $key => $field) {
      $field_options[$key] = $key;
    }

    $form['mysqlgis_entity_from_url_field'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Source Field'),
      '#default_value' => $views_plugin->options['mysqlgis_entity_from_url_field'],
      '#options' => $field_options,
      '#dependency' => array(
        'edit-options-source' => array('mysqlgis_entity_from_url'),
      ),
    );
  }

  /**
   * Set up the where clause for the contextual filter argument.
   */
  public function query($group_by = FALSE, &$views_plugin) {
    $entity_type =  $entity_id = $field_name = '';
    if (empty($views_plugin->options['mysqlgis_entity_from_url_entity_type'])) {
      return;
    }
    $entity_type = $views_plugin->options['mysqlgis_entity_from_url_entity_type'];
    if (empty($views_plugin->options['mysqlgis_entity_from_url_field'])) {
      return;
    }
    $field_name = $views_plugin->options['mysqlgis_entity_from_url_field'];
    if (empty($views_plugin->argument)) {
      return;
    }
    $entity_id = intval($views_plugin->argument);
    $entities = entity_load($entity_type, array($entity_id));
    if (empty($entities)) {
      return;
    }
    $entity = reset($entities);
    dpm($entity);
  }
}
