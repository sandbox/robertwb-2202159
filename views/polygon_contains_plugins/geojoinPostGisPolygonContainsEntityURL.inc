<?php

/**
 * @file
 * Contains geojoinPolygonContainsEntityURL.
 * @category geojoin_views_handler_argument_polygoncontains
 *   Adds "Polygon Contains Points" contextual filter.
 */

class geojoinPostGisPolygonContainsEntityURL extends geojoinPolygonContainsBase implements geojoinPolygonContainsPluginInterface {
  public function option_definition(&$options, $views_plugin) {
    $options['PostGis_entity_from_url_entity_type'] = array(
      'default' => 'node',
    );
    $options['PostGis_entity_from_url_field'] = array(
      'default' => '',
    );
    $options['PostGis_entity_from_url_entity_targetbundle'] = array(
      'default' => '',
    );
    $options['PostGis_entity_from_url_targetfield'] = array(
      'default' => '',
    );
  }

  public function options_form(&$form, &$form_state, $views_plugin) {
    $entities = entity_get_info();
    $entity_options = array();
    foreach ($entities as $key => $entity) {
      $entity_options[$key] = $entity['label'];
    }

    $form['PostGis_entity_from_url_entity_type'] = array(
      '#type' => 'select',
      '#title' => t('Entity Type, Polygon'),
      '#default_value' => $views_plugin->options['PostGis_entity_from_url_entity_type'],
      '#options' => $entity_options,
      '#dependency' => array(
        'edit-options-source' => array('PostGis_entity_from_url'),
      ),
    );
    $geofields = _geofield_get_geofield_fields();
    $field_options = array();
    foreach ($geofields as $key => $field) {
      $field_options[$key] = $key;
    }

    $form['PostGis_entity_from_url_field'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Source Field, Polygon'),
      '#default_value' => $views_plugin->options['PostGis_entity_from_url_field'],
      '#options' => $field_options,
      '#dependency' => array(
        'edit-options-source' => array('PostGis_entity_from_url'),
      ),
    );

    $base_table = $views_plugin->view->base_table;
    $target_bundle_entity_options = array();
    // one a view that is based on node, the points should be based only on node.
    foreach ($entities[$base_table]['bundles'] as $bundle_name => $bundle) {
      $target_bundle_entity_options[$bundle_name] = $bundle['label'];
    }

    $form['PostGis_entity_from_url_entity_targetbundle'] = array(
      '#type' => 'select',
      '#title' => t('Bundle, Points'),
      '#description' => t('The view is based on ' . $base_table . ' so are the Points Entity Type'),
      '#required' => TRUE,
      '#default_value' => $views_plugin->options['PostGis_entity_from_url_entity_targetbundle'],
      '#options' => $target_bundle_entity_options,
      '#dependency' => array(
        'edit-options-source' => array('PostGis_entity_from_url'),
      ),
    );

    $form['PostGis_entity_from_url_targetfield'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Source Field, Points'),
      '#default_value' => $views_plugin->options['PostGis_entity_from_url_targetfield'],
      '#options' => $field_options,
      '#dependency' => array(
        'edit-options-source' => array('PostGis_entity_from_url'),
      ),
    );
  }

  /**
   * Set up the where clause for the contextual filter argument.
   */
  /* -- Delete this line if you want to use this function
  public function query($group_by = FALSE, &$views_plugin) {
    // $group_by is the filter parameter
    drupal_set_message("PostGis Plugin Query Called");
    $entity_type =  $entity_id = $field_name = '';
    if (empty($views_plugin->options['PostGis_entity_from_url_entity_type'])) {
      drupal_set_message("PostGis_entity_from_url_entity_type not set");
      return;
    }
    $entity_type = $views_plugin->options['PostGis_entity_from_url_entity_type'];
    if (empty($views_plugin->options['PostGis_entity_from_url_field'])) {
      drupal_set_message("PostGis_entity_from_url_field options not set");
      drupal_set_message(print_r($views_plugin->options,1));
      return;
    }
    $field_name = $views_plugin->options['PostGis_entity_from_url_field'];
    if (empty($views_plugin->argument)) {
      drupal_set_message("PostGis_entity_from_url_field argument not set");
      return;
    }
    drupal_set_message("PostGis Plugin -- All Config Checks Passed ");
    // verify geometry with GeoPHP
    // skipped for now
    $verified_geo = $views_plugin->argument;
    // add the where clause
    // fieldref needs to contain the table alias.field_name
    // also needs to verify that GeoField version is using Binary not WKT
    // but for now just go with field_name + geom and pray for a simple view...
    $fieldref = $field_name . '_geom';
    // this first part of clause invokes spatial index in PostGIS
    $clause = " $fieldref && geomFromText('$verified_geo') ";
    // should check, some oldschool PGis don't use st_ prefix
    // the same goes for some old school (5.0ish) mysql installs
    $clause .= " AND st_contains(geomFromText('$verified_geo'), $fieldref)";
    drupal_set_message("Clause: $clause");
  }
  // */

  /**
   * Set up the where clause for the contextual filter argument.
   */
  public function query($group_by = FALSE, &$views_plugin) {
    $polygon_entity_type =  $polygon_entity_id = $polygon_field_name = '';
    $points_entity_type = $points_entity_bundle = $points_field_name = '';
    if (empty($views_plugin->options['PostGis_entity_from_url_entity_type'])) {
      return;
    }
    // the entity url's entity type, can probably be automatically detected.
    // but algorithm for that is dicey.. e.g. node/1 , taxonomy-term/2
    // so in node/1 example $views_plugin->argument is 1, so taxonomy-term/2 the view plugin arguement is 2
    // but its dicey to detect what arg to get either "node" or "taxonomy-term".
    // since arg position is not usually given here in the view plugin..
    // so we depend on site admin to provide entity type
    $polygon_entity_type = $views_plugin->options['PostGis_entity_from_url_entity_type'];
    if (empty($views_plugin->options['PostGis_entity_from_url_field'])) {
      return;
    }
    // the entity url's geofield field name
    $polygon_field_name = $views_plugin->options['PostGis_entity_from_url_field'];
    if (empty($views_plugin->view->base_table)) {
      return;
    }
    // the idea is the points inside the polygon will depend on the base table of the view.
    $points_entity_type = $views_plugin->view->base_table;
    if (empty($views_plugin->options['PostGis_entity_from_url_entity_targetbundle'])) {
      return;
    }
    // probably optional form, but was needed in geophp algorithm
    $points_entity_bundle = $views_plugin->options['PostGis_entity_from_url_entity_targetbundle'];
    if (empty($views_plugin->options['PostGis_entity_from_url_targetfield'])) {
      return;
    }
    // depend on site-admin to provide the point's geofield.
    $points_field_name = $views_plugin->options['PostGis_entity_from_url_targetfield'];
    if (empty($views_plugin->argument)) {
      return;
    }
    $points_entity_type = check_plain($points_entity_type);
    $points_entity_bundle = check_plain($points_entity_bundle);
    $points_field_name = check_plain($points_field_name);
    $polygon_entity_id = intval($views_plugin->argument);
    // verify geometry with GeoPHP
    // skipped for now
    $verified_geo = $views_plugin->argument;
    // add the where clause
    // fieldref needs to contain the table alias.field_name
    // also needs to verify that GeoField version is using Binary not WKT
    // but for now just go with field_name + geom and pray for a simple view...
    $fieldref = $field_name . '_geom';
    // this first part of clause invokes spatial index in PostGIS
    $clause = " $fieldref && geomFromText('$verified_geo') ";
    // should check, some oldschool PGis don't use st_ prefix
    // the same goes for some old school (5.0ish) mysql installs
    $clause .= " AND st_contains(geomFromText('$verified_geo'), $fieldref)";
    drupal_set_message("Clause: $clause");
  }
}
